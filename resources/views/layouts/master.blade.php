<!DOCTYPE html>
<html>
<head>
	<title>@yield('title')</title>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  	<script type="text/javascript">
  	function countQnt()
	{
  		$.ajax({
	    	url: "../countno/", 
	    	type: "GET",
	    	success: function(result)
	    	{
	    		$("#cnt").html(result);
	    	}
		});
  	}
  	</script>
</head>
<body style="background-color: lavender" onload="countQnt()">
	<nav class="navbar navbar-default navbar-fixed-top">
	  	<div class="container-fluid">
	    	<div class="navbar-header">
	      		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      	</button>
	      		<a class="navbar-brand" href="#">WebSiteName</a>
	    	</div>
	    	<div class="collapse navbar-collapse" id="myNavbar">
				<ul class="nav navbar-nav">
					<li class="active"><a href="/">Home</a></li>
					<li><a href="#">Page 1</a></li>
					<li><a href="#">Page 2</a></li>
					<li><a href="#">Page 3</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li><a href="../chart"><span class="glyphicon glyphicon-shopping-cart"><span class="badge" id="cnt"></span></span></a></li>
					<?php $value = session('uid');?>
            		@if(!empty($value))
            			<li><a href="../lists"><span class="glyphicon glyphicon-list-alt"></span> Shopping Lists</a></li>
            			<li><a href="../logout"><span class="glyphicon glyphicon-log-out"></span> Log Out</a></li>
            		@else
						<li><a href="#" data-toggle="modal" data-target="#signup"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
						<li><a href="#" data-toggle="modal" data-target="#login"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>

					@endif
				</ul>
	    	</div>
	  </div>
	</nav><br/><br/>

	<!--===========================================login form==========================================-->
	<div class="container">
		<div class="modal fade" id="login" tabindex="-1" role="dialog">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h3 class="modal-title" id="myModalLabel">Log In</h3>
					</div>
					<div class="modal-body">
						<form name="login" action="login" method="post" id="login">
			          		<div class="form-group">
			            		<input type="email" name="username" class="form-control tfild" id="username" required placeholder="Username,Email"><br/>
			            		<input type="password" name="password" class="form-control tfild" id="password" required placeholder="Password">
			            		<input type="hidden" name="_token" value="{{csrf_token()}}">
			          		</div>
			          		<div class="modal-footer">
					  			<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
					  			<input type="submit" name="login" class="btn btn-primary" value="Log In">
							</div>
			       		</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!--===========================================signup form==========================================-->
	<div class="container">
		<div class="modal fade" id="signup" tabindex="-1" role="dialog">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h3 class="modal-title" id="myModalLabel">Sign Up</h3>
					</div>
					<div class="modal-body">
						<form name="signup" action="signup" method="post" id="signup">
			          		<div class="form-group">
			          			<input type="text" name="fullname" class="form-control tfild" id="fullname" required placeholder="Fullname"><br/>
			            		<input type="email" name="username" class="form-control tfild" id="username" required placeholder="Username,Email"><br/>
			            		<input type="password" name="password" class="form-control tfild" id="password" required placeholder="Password">
			            		<input type="hidden" name="_token" value="{{csrf_token()}}">
			          		</div>
			          		<div class="modal-footer">
					  			<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
					  			<input type="submit" name="signup" class="btn btn-primary" value="Sign Up">
							</div>
			       		</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="panel panel-default">
		  	<div class="panel-body">
		  		@yield('content')
		  	</div>
		  	<div class="panel-body">
		  		@yield('paginate')
		  	</div>
		</div>
	</div>
	@yield('footer')
</body>
</html>