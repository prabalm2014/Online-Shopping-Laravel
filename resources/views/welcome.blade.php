@extends('layouts.master')

@section('title', 'Home')

@section('content')
    <br/>
    @foreach ($str as $value)
    <div class="panel panel-default col-md-3" style="margin-right: 2%;margin-left: 5%;">
        <div class="panel-body">
            <h3>{{$value['productname']}}</h3>
            <h5>{{$value['type']}}</h5>
            <h4>{{$value['price']}} TK</h4>
            @if(!empty(session('uid')))
            <hr>
                <p><a href="addtochart/{{$value['pid']}}/{{$value['productname']}}/{{$value['price']}}" class="btn btn-primary" role="button">Add to Chart</a></p>
            @else
            <hr>
            <h6 style="color: blue;">* Login To Shop</h6>
            @endif
        </div>
    </div>
    @endforeach
@endsection

@section('paginate')
    <div style="text-align: center;">{{$str->links()}}</div>
@endsection

@section('footer')
    
@endsection
