@extends('layouts.master')

@section('title', 'My Chart')

@section('content')
<br/>
    <div class="panel panel-default col-md-10 col-md-offset-1">
        <div class="panel-body">
        <table class="table table-striped">
        	<tr>
        		<th>Product Name</th>
        		<th>Quantity</th>
        		<th>Price</th>
        		<th>Order ID</th>
        		<th>Product ID</th>
        		<th>Order Time</th>
        	</tr>
        	@foreach ($str as $value)
        	<tr>
        		<td>{{$value['productname']}}</td>
        		<td>{{$value['quantity']}}</td>
        		<td>{{$value['price']}} TK</td>
        		<td>{{$value['oid']}}</td>
        		<td>{{$value['pid']}}</td>
        		<td>{{$value['order_time']}}</td>
        	</tr>
        	@endforeach
        	<tr>
        		<th></th>
        		<th>Total Quantity</th>
        		<th>Total Price</th>
        		<th></th>
        		<th></th>
        		<th></th>
        	</tr>
        	<tr>
        		<td></td>
        		<td>{{$cs}}</td>
        		<td>{{$prc}} TK</td>
        		<td></td>
        		<td></td>
        		<td></td>
        	</tr>
        </table>
        <a href="/" class="btn btn-primary" role="button">Back</a>
        <a href="../clear" class="btn btn-danger" role="button">Clear Chart</a>
        <a href="../buy" class="btn btn-primary" role="button">Buy</a>
        </div>
    </div>
@endsection

@section('footer')
    <!---->
@endsection