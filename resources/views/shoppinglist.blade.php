@extends('layouts.master')

@section('title', 'Shopping Lists')

@section('content')
<br/>
    <div class="panel panel-default col-md-10 col-md-offset-1">
        <div class="panel-body">
            @foreach($str as $value)
                <div class="well well-lg">
                    <span>Hello,</span>
                    <h6 style="color: blue;">Dear {{$nam['fullname']}}<h6>
                    <p>
                        You order successfuly added to our system. We will deliver your product within your desire time.Please check your order again...
                        <ul style="color: blue;">
                            <li>Total Quantity: {{$value['totalquantity']}}</li>
                            <li>Total Price: {{$value['totalprice']}} TK</li>
                            <li>Order Date and Time: {{$value['todate']}}</li>
                        </ul>
                    </p>
                </div>
            @endforeach
            <a href="" class="btn btn-primary" role="button">Back</a>
        </div>
    </div>
@endsection

@section('footer')
    <!---->
@endsection