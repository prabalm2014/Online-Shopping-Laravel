<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orderproduct extends Model
{
    protected $table = 'orderproduct';
    protected $guarded = ['oid','updated_at', 'created_at'];
}
