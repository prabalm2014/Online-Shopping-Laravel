<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Mproduct;
use App\Orderproduct;
use App\Orderbackup;
use App\Users;
use App\Report;

class Product extends Controller
{
	public function index()
    {
	    $str = Mproduct::paginate(9);
	    return view('welcome', compact('str'));
	}
	public function addchart(Request $request)
    {
    	$uid = session('uid');
    	$orderproduct = new Orderproduct;
    	$orderproduct->productname = $request->productname;
    	$orderproduct->price = $request->price;
    	$orderproduct->pid = $request->pid;
    	$orderproduct->uid = $uid;
    	date_default_timezone_set("Asia/Dhaka");
    	$orderproduct->order_time = date("Y-m-d H:I:s");//2016-10-20 05:32:43
	    if($orderproduct->save())
	    {
    		return redirect()->back();
	    }
	}
	public function showchart(Request $request)
	{
		$uid = session('uid');
		$str = Orderproduct::where('uid', $uid)->get();
		$cs = Orderproduct::where('uid', $uid)->count();
		$prc = Orderproduct::where('uid', $uid)->sum('price');
		return view('mychart', compact('str', 'cs', 'prc'));
	}

	public function crtshow(Request $request)
	{
		$uid = session('uid');
		$cs = Orderproduct::where('uid', $uid)->count();
		echo $cs;
	}

	public function backup()
	{
		$uid = session('uid');
		$orderproduct = new Orderproduct;
		$str = Orderproduct::all();
		foreach ($str as $id => $value) 
		{
			$orderbackup = new Orderbackup;
    		$orderbackup->productname = $value['productname'];
	    	$orderbackup->price = $value['price'];
	    	$orderbackup->pid = $value['pid'];
	    	$orderbackup->uid = $value['uid'];
	    	$orderbackup->order_time = $value['order_time'];   
			$orderbackup->save();
		}
		$cs = Orderproduct::where('uid', $uid)->count();
		$prc = Orderproduct::where('uid', $uid)->sum('price');

		$report = new Report;
		$report->totalquantity = $cs;
    	$report->totalprice = $prc;
    	$report->uid = $uid;   
    	date_default_timezone_set("Asia/Dhaka");
    	$report->todate = date("Y-m-d H:I:s");
		$report->save();

		$str = Orderproduct::where('uid', $uid)->delete();
	    return redirect('');
	}
	public function clear()
	{
		$uid = session('uid');
		$str = Orderproduct::where('uid', $uid)->delete();
		return redirect('');
	}

	public function signup(Request $request)
	{
		$users = new Users;
		$users->fullname = $request->fullname;
		$users->email = $request->username;
		$users->password = $request->password;
		$name = $request->username;
		if($users->save())
		{
			$str = Users::where('email', $name)->first();
    		$uid = $str['uid'];
    		session(['uid'=> $uid]);
			return redirect('');
		}
	}

	public function login(Request $request)
	{
		$users = new Users;
		$uname = $request->username;
    	$pass = $request->password;
    	$login = Users::where('email', $uname)->where('password', $pass)->first();
    	if($login)
    	{
    		$str = Users::where('email', $uname)->first();
    		$uid = $str['uid'];
    		session(['uid'=> $uid]);
			return redirect('');
    	}
    	else
    	{
    		echo "<h1>Username and Password not matched!</h1>";
    	}
	}

	public function logout(Request $request)
	{
		$request->session()->forget('uid');
		return redirect('');
	}

	public function shoplist()
	{
		$uid = session('uid');
		$str = Report::all()->where('uid', $uid);
		$nam = Users::where('uid', $uid)->first();
		return view('shoppinglist', compact('str', 'nam'));
	}
}
