<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mproduct extends Model
{
    protected $table = 'product';
    protected $guarded = ['pid','updated_at', 'created_at'];
}

