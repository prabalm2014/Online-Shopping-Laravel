<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $table = 'report';
    protected $guarded = ['rid', 'updated_at', 'created_at'];
}
