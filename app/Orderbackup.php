<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orderbackup extends Model
{
    protected $table = 'orderproduct_backup';
    protected $guarded = ['oid','updated_at', 'created_at'];
}
