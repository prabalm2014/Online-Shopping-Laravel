<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('', 'Product@index');
Route::get('addtochart/{pid}/{productname}/{price}', 'Product@addchart');
Route::get('chart', 'Product@showchart');//add user id
Route::get('buy', 'Product@backup');
Route::get('countno', 'Product@crtshow');
Route::get('clear', 'Product@clear');
Route::post('login', 'Product@login');
Route::get('logout', 'Product@logout');
Route::get('lists', 'Product@shoplist');
Route::post('signup', 'Product@signup');