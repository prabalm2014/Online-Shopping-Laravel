-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 23, 2016 at 10:19 AM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `larapro`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orderproduct`
--

CREATE TABLE `orderproduct` (
  `oid` int(15) NOT NULL,
  `productname` varchar(255) NOT NULL,
  `quantity` int(1) NOT NULL DEFAULT '1',
  `price` int(255) NOT NULL,
  `pid` int(15) NOT NULL,
  `uid` int(15) NOT NULL,
  `order_time` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL,
  `created_at` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `orderproduct_backup`
--

CREATE TABLE `orderproduct_backup` (
  `oid` int(15) NOT NULL,
  `productname` varchar(255) NOT NULL,
  `quantity` int(1) NOT NULL DEFAULT '1',
  `price` varchar(255) NOT NULL,
  `pid` int(15) NOT NULL,
  `uid` int(15) NOT NULL,
  `order_time` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL,
  `created_at` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orderproduct_backup`
--

INSERT INTO `orderproduct_backup` (`oid`, `productname`, `quantity`, `price`, `pid`, `uid`, `order_time`, `updated_at`, `created_at`) VALUES
(1, 'Tshirt', 1, '380', 2, 2, '2016-10-23 08:00:42', '2016-10-23 02:03:05', '2016-10-23 02:03:05'),
(2, 'Pant ', 1, '699', 3, 2, '2016-10-23 08:00:44', '2016-10-23 02:03:05', '2016-10-23 02:03:05'),
(3, 'Cap', 1, '399', 6, 2, '2016-10-23 08:00:45', '2016-10-23 02:03:05', '2016-10-23 02:03:05'),
(4, 'Cap', 1, '299', 5, 2, '2016-10-23 08:00:46', '2016-10-23 02:03:05', '2016-10-23 02:03:05'),
(5, 'Pant', 1, '899', 4, 2, '2016-10-23 08:00:47', '2016-10-23 02:03:05', '2016-10-23 02:03:05'),
(6, 'Sari', 1, '2500', 7, 2, '2016-10-23 08:00:49', '2016-10-23 02:03:05', '2016-10-23 02:03:05'),
(7, 'Cap', 1, '399', 6, 2, '2016-10-23 08:00:50', '2016-10-23 02:03:06', '2016-10-23 02:03:06'),
(8, 'Tshirt', 1, '380', 2, 2, '2016-10-23 08:00:53', '2016-10-23 02:03:06', '2016-10-23 02:03:06'),
(9, 'Pant ', 1, '699', 3, 2, '2016-10-23 08:00:30', '2016-10-23 02:03:36', '2016-10-23 02:03:36'),
(10, 'Pant', 1, '899', 4, 2, '2016-10-23 08:00:31', '2016-10-23 02:03:37', '2016-10-23 02:03:37'),
(11, 'Tshirt', 1, '280', 1, 2, '2016-10-23 08:00:32', '2016-10-23 02:03:37', '2016-10-23 02:03:37'),
(12, 'Cap', 1, '299', 5, 2, '2016-10-23 08:00:33', '2016-10-23 02:03:37', '2016-10-23 02:03:37'),
(13, 'Tshirt', 1, '280', 1, 1, '2016-10-23 08:00:07', '2016-10-23 02:04:19', '2016-10-23 02:04:19'),
(14, 'Pant', 1, '899', 4, 1, '2016-10-23 08:00:08', '2016-10-23 02:04:19', '2016-10-23 02:04:19'),
(15, 'Jamdani Sari', 1, '6500', 8, 1, '2016-10-23 08:00:10', '2016-10-23 02:04:19', '2016-10-23 02:04:19'),
(16, 'Sari', 1, '2500', 7, 1, '2016-10-23 08:00:12', '2016-10-23 02:04:19', '2016-10-23 02:04:19'),
(17, 'Cap', 1, '399', 6, 1, '2016-10-23 08:00:14', '2016-10-23 02:04:19', '2016-10-23 02:04:19'),
(18, 'Pant ', 1, '699', 3, 1, '2016-10-23 08:00:22', '2016-10-23 02:04:29', '2016-10-23 02:04:29'),
(19, 'Pant', 1, '899', 4, 1, '2016-10-23 08:00:23', '2016-10-23 02:04:29', '2016-10-23 02:04:29'),
(20, 'Sari', 1, '2500', 7, 1, '2016-10-23 08:00:24', '2016-10-23 02:04:29', '2016-10-23 02:04:29'),
(21, 'Cap', 1, '399', 6, 1, '2016-10-23 08:00:26', '2016-10-23 02:04:29', '2016-10-23 02:04:29'),
(22, 'Pant ', 1, '699', 3, 3, '2016-10-23 08:00:59', '2016-10-23 02:08:16', '2016-10-23 02:08:16'),
(23, 'Cap', 1, '299', 5, 3, '2016-10-23 08:00:00', '2016-10-23 02:08:16', '2016-10-23 02:08:16'),
(24, 'Tshirt', 1, '380', 2, 3, '2016-10-23 08:00:01', '2016-10-23 02:08:16', '2016-10-23 02:08:16'),
(25, 'Pant', 1, '899', 4, 3, '2016-10-23 08:00:02', '2016-10-23 02:08:16', '2016-10-23 02:08:16'),
(26, 'Jeans', 1, '2500', 14, 3, '2016-10-23 08:00:05', '2016-10-23 02:08:16', '2016-10-23 02:08:16'),
(27, 'Hijab & Scarf', 1, '700', 13, 3, '2016-10-23 08:00:09', '2016-10-23 02:08:16', '2016-10-23 02:08:16'),
(28, 'Cap', 1, '299', 5, 3, '2016-10-23 08:00:25', '2016-10-23 02:08:17', '2016-10-23 02:08:17'),
(29, 'Shorts', 1, '400', 11, 3, '2016-10-23 08:00:32', '2016-10-23 02:08:17', '2016-10-23 02:08:17'),
(30, 'Jeans', 1, '2500', 14, 3, '2016-10-23 08:00:12', '2016-10-23 02:08:17', '2016-10-23 02:08:17'),
(31, 'Tops', 1, '399', 12, 3, '2016-10-23 08:00:58', '2016-10-23 02:08:17', '2016-10-23 02:08:17'),
(32, 'Tshirt', 1, '280', 1, 3, '2016-10-23 08:00:25', '2016-10-23 02:08:34', '2016-10-23 02:08:34'),
(33, 'Cap', 1, '299', 5, 3, '2016-10-23 08:00:25', '2016-10-23 02:08:34', '2016-10-23 02:08:34'),
(34, 'Pant', 1, '899', 4, 3, '2016-10-23 08:00:26', '2016-10-23 02:08:34', '2016-10-23 02:08:34'),
(35, 'Jamdani Sari', 1, '6500', 8, 3, '2016-10-23 08:00:28', '2016-10-23 02:08:34', '2016-10-23 02:08:34'),
(36, 'Pant ', 1, '699', 3, 3, '2016-10-23 08:00:30', '2016-10-23 02:08:34', '2016-10-23 02:08:34'),
(37, 'Cap', 1, '399', 6, 3, '2016-10-23 08:00:31', '2016-10-23 02:08:34', '2016-10-23 02:08:34'),
(38, 'Tshirt', 1, '280', 1, 3, '2016-10-23 08:00:37', '2016-10-23 02:08:45', '2016-10-23 02:08:45'),
(39, 'Pant', 1, '899', 4, 3, '2016-10-23 08:00:37', '2016-10-23 02:08:45', '2016-10-23 02:08:45'),
(40, 'Cap', 1, '399', 6, 3, '2016-10-23 08:00:39', '2016-10-23 02:08:45', '2016-10-23 02:08:45'),
(41, 'Pant ', 1, '699', 3, 3, '2016-10-23 08:00:39', '2016-10-23 02:08:45', '2016-10-23 02:08:45'),
(42, 'Tshirt', 1, '380', 2, 3, '2016-10-23 08:00:40', '2016-10-23 02:08:45', '2016-10-23 02:08:45'),
(43, 'Cap', 1, '299', 5, 3, '2016-10-23 08:00:41', '2016-10-23 02:08:45', '2016-10-23 02:08:45'),
(44, 'Pant ', 1, '699', 3, 1, '2016-10-23 08:00:05', '2016-10-23 02:13:23', '2016-10-23 02:13:23'),
(45, 'Cap', 1, '299', 5, 1, '2016-10-23 08:00:08', '2016-10-23 02:13:23', '2016-10-23 02:13:23'),
(46, 'Shorts', 1, '400', 11, 1, '2016-10-23 08:00:11', '2016-10-23 02:13:23', '2016-10-23 02:13:23'),
(47, 'Panjabi Payjama', 1, '1900', 10, 1, '2016-10-23 08:00:13', '2016-10-23 02:13:23', '2016-10-23 02:13:23'),
(48, 'Tops', 1, '399', 12, 1, '2016-10-23 08:00:16', '2016-10-23 02:13:23', '2016-10-23 02:13:23'),
(49, 'Tshirt', 1, '280', 1, 1, '2016-10-23 10:00:29', '2016-10-23 04:12:40', '2016-10-23 04:12:40'),
(50, 'Tshirt', 1, '380', 2, 1, '2016-10-23 10:00:30', '2016-10-23 04:12:40', '2016-10-23 04:12:40'),
(51, 'Pant ', 1, '699', 3, 1, '2016-10-23 10:00:31', '2016-10-23 04:12:40', '2016-10-23 04:12:40');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `pid` int(15) NOT NULL,
  `productname` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  `updated_at` timestamp NOT NULL,
  `created_at` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`pid`, `productname`, `type`, `price`, `updated_at`, `created_at`) VALUES
(1, 'Tshirt', 'Male', 280, '2016-10-19 09:41:15', '2016-10-19 09:41:15'),
(2, 'Tshirt', 'Female', 380, '2016-10-19 09:41:15', '2016-10-19 09:41:15'),
(3, 'Pant ', 'Male', 699, '2016-10-19 09:41:15', '2016-10-19 09:41:15'),
(4, 'Pant', 'Female', 899, '2016-10-19 09:41:15', '2016-10-19 09:41:15'),
(5, 'Cap', 'Male', 299, '2016-10-19 09:45:24', '2016-10-19 09:45:24'),
(6, 'Cap', 'Female', 399, '2016-10-19 09:45:24', '2016-10-19 09:45:24'),
(7, 'Sari', 'Female', 2500, '2016-10-19 09:45:24', '2016-10-19 09:45:24'),
(8, 'Jamdani Sari', 'Female', 6500, '2016-10-19 09:45:24', '2016-10-19 09:45:24'),
(9, 'Polo', 'Male', 500, '2016-10-19 09:45:24', '2016-10-19 09:45:24'),
(10, 'Panjabi Payjama', 'Male', 1900, '2016-10-19 09:49:54', '2016-10-19 09:49:54'),
(11, 'Shorts', 'Male', 400, '2016-10-19 09:49:54', '2016-10-19 09:49:54'),
(12, 'Tops', 'Female', 399, '2016-10-19 09:49:54', '2016-10-19 09:49:54'),
(13, 'Hijab & Scarf', 'Female', 700, '2016-10-19 09:49:54', '2016-10-19 09:49:54'),
(14, 'Jeans', 'Male', 2500, '2016-10-19 09:49:54', '2016-10-19 09:49:54');

-- --------------------------------------------------------

--
-- Table structure for table `report`
--

CREATE TABLE `report` (
  `rid` int(15) NOT NULL,
  `totalquantity` int(200) NOT NULL,
  `totalprice` int(200) NOT NULL,
  `uid` int(15) NOT NULL,
  `todate` datetime NOT NULL,
  `updated_at` timestamp NOT NULL,
  `created_at` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `report`
--

INSERT INTO `report` (`rid`, `totalquantity`, `totalprice`, `uid`, `todate`, `updated_at`, `created_at`) VALUES
(1, 8, 5955, 2, '2016-10-23 14:00:06', '2016-10-23 08:03:06', '2016-10-23 08:03:06'),
(2, 4, 2177, 2, '2016-10-23 14:00:37', '2016-10-23 08:03:37', '2016-10-23 08:03:37'),
(3, 5, 10578, 1, '2016-10-23 14:00:19', '2016-10-23 08:04:19', '2016-10-23 08:04:19'),
(4, 4, 4497, 1, '2016-10-23 14:00:29', '2016-10-23 08:04:29', '2016-10-23 08:04:29'),
(5, 10, 9075, 3, '2016-10-23 14:00:17', '2016-10-23 08:08:17', '2016-10-23 08:08:17'),
(6, 6, 9076, 3, '2016-10-23 14:00:34', '2016-10-23 08:08:34', '2016-10-23 08:08:34'),
(7, 6, 2956, 3, '2016-10-23 14:00:45', '2016-10-23 08:08:45', '2016-10-23 08:08:45'),
(8, 5, 3697, 1, '2016-10-23 14:00:23', '2016-10-23 08:13:23', '2016-10-23 08:13:23'),
(9, 3, 1359, 1, '2016-10-23 16:00:40', '2016-10-23 10:12:40', '2016-10-23 10:12:40');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `uid` int(15) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `updated_at` timestamp NOT NULL,
  `created_at` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`uid`, `fullname`, `email`, `password`, `updated_at`, `created_at`) VALUES
(1, 'Kalam', 'km@gmail.com', '123456', '2016-10-23 02:01:34', '2016-10-23 02:01:34'),
(2, 'Hamid', 'hm@gmail.com', '123456', '2016-10-23 02:01:53', '2016-10-23 02:01:53'),
(3, 'Rafiq', 'rf@gmail.com', '123456', '2016-10-23 02:02:22', '2016-10-23 02:02:22');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orderproduct`
--
ALTER TABLE `orderproduct`
  ADD PRIMARY KEY (`oid`),
  ADD KEY `pid` (`pid`),
  ADD KEY `uid` (`uid`);

--
-- Indexes for table `orderproduct_backup`
--
ALTER TABLE `orderproduct_backup`
  ADD PRIMARY KEY (`oid`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`pid`);

--
-- Indexes for table `report`
--
ALTER TABLE `report`
  ADD PRIMARY KEY (`rid`),
  ADD KEY `uid` (`uid`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`uid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `orderproduct`
--
ALTER TABLE `orderproduct`
  MODIFY `oid` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;
--
-- AUTO_INCREMENT for table `orderproduct_backup`
--
ALTER TABLE `orderproduct_backup`
  MODIFY `oid` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `pid` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `report`
--
ALTER TABLE `report`
  MODIFY `rid` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `uid` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `orderproduct`
--
ALTER TABLE `orderproduct`
  ADD CONSTRAINT `orderproduct_ibfk_1` FOREIGN KEY (`pid`) REFERENCES `product` (`pid`),
  ADD CONSTRAINT `orderproduct_ibfk_2` FOREIGN KEY (`uid`) REFERENCES `user` (`uid`);

--
-- Constraints for table `report`
--
ALTER TABLE `report`
  ADD CONSTRAINT `report_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `user` (`uid`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
